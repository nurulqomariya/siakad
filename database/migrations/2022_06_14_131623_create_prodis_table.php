<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prodis', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_jenjang');
            $table->string('name');
            $table->string('akreditasi');
            $table->string('tgl_berdiri');
            $table->string('email');
            $table->string('keterangan');
            $table->timestamps();

            $table->foreign('id_jenjang')->references('id')->on('jenjangs')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prodis');
    }
};
