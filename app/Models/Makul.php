<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Makul extends Model
{
    protected $fillable = [
        'id_prodi', 'name', 'jenis', 'sks', 'semester',
    ];
}
