<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Prodi extends Model
{
    protected $fillable = [
        'id_jenjang', 'name', 'akreditasi', 'tgl_berdiri', 'email', 'keterangan', 
    ];
}
